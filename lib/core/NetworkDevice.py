#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#  Author: Elliott Thompson
#
#  Wireless Interface, Physical Interface, Network Interface
#  Wired Interface. Card, interface_name, network device
#  Micasa Interface
#


import pyric                    # pyric errors
import pyric.pyw as pyw         # iw functionality
import pyric.utils.hardware as hw   # hardware functions
import netifaces as ni          # wired functionality
from mac_vendor_lookup import MacLookup

class NetworkDevice:

    def __init__(self, interface_name):

        self.interface_name = ""
        self.media_type = ""
        self.wireless_modes = []
        self.wireless_bands = []
        self.mac = ""
        self.ip = ""
        self.vendor = ""
        self.chipset = ""
        self.can_inject = False
        self.is_up = False
        self.is_connected = False

        self.interface_name = interface_name

        if pyw.iswireless(self.interface_name):
            self.media_type = 'wireless'

            network_card = pyw.getcard(self.interface_name)

            self.is_up = pyw.isup(network_card)
        
            card_physical_info = pyw.phyinfo(network_card)

            self.chipset = hw.ifcard(self.interface_name)[1]
            if self.chipset in ['AR9001/9002/9271', 'Ralink RT2870/3070']:
                self.can_inject = True

            for wireless_mode in card_physical_info['modes']:
                if wireless_mode == 'managed':
                    self.wireless_modes.append('managed')
                if wireless_mode == 'AP':
                    self.wireless_modes.append('AP')
                if wireless_mode == 'monitor':
                    self.wireless_modes.append('monitor')
        
            for wireless_band in card_physical_info['bands']:
                if wireless_band == '2GHz':
                    self.wireless_bands.append('2GHz')
                if wireless_band == '5GHz':
                    self.wireless_bands.append('5GHz')


            network_interface_info = pyw.ifinfo(network_card)
            
            if network_interface_info['hwaddr']:
                self.mac = network_interface_info['hwaddr']
                try:
                    self.vendor = MacLookup().lookup(self.mac)
                except: 
                    self.vendor = 'unknown'

            try:
                self.ip = network_interface_info['inet']

                if self.ip == None:
                    raise Exception('No IP Assigned')
                
                self.is_connected = True
            except:
                self.is_connected = False
            

        else:
            self.media_type = 'wired'
            network_interface_info = ni.ifaddresses(self.interface_name)

            if network_interface_info[ni.AF_LINK][0]['addr']:
                self.mac = network_interface_info[ni.AF_LINK][0]['addr']
                try:
                    self.vendor = MacLookup().lookup(self.mac)
                except: 
                    self.vendor = 'unknown'

            try:
                self.ip = network_interface_info[ni.AF_INET][0]['addr']
                self.is_connected = True

                if self.ip == "127.0.0.1":
                    self.media_type = "localhost"
                    self.is_connected = False

                self.is_up = True # Doesn't effect workflow on wired networks
            except:
                self.is_connected = False
                self.is_up = False